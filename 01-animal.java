/*
1) Criar mais cinco classes que extendam a classe Animal.
2) Criar duas classes que extendam a classe Cachorro.
3) Instanciar os objetos e mostrar o funcionamento.
*/

class AnimalExemplo{
	
	public static void main(String[] args){
		Gato g = new Gato();
		Cachorro c = new Cachorro();

		//Animal animal = g;
		Animal animal = c;
		animal.fazerBarulho();
	}
}

class Animal{
	
	public Animal(){

	}

	public void fazerBarulho(){
		System.out.println("-----");
	}
}

class Cachorro extends Animal{
	public Cachorro(){

	}
	@Override
	public void fazerBarulho(){
		System.out.println("au au");
	}
}

class Gato extends Animal{
	public Gato(){

	}
	@Override
	public void fazerBarulho(){
		System.out.println("miau");
	}
}