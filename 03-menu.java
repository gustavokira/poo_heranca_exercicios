/*
	1) Termine o menu alternativo.
	2) Crie um novo tipo de menu.
	3) Faça com que seja possível escolher o seu menu.
*/

import java.util.*;

class MenuExemplo{
	
	public static void main(String[] args){

		Menu menu = null;

		Scanner s = new Scanner(System.in);

		System.out.println("escolha o seu menu!");
		System.out.println("1) normal");
		System.out.println("2) alternativo");

	    int qualMenu = s.nextInt();
	    
	    switch (qualMenu) {
	    	case Menu.NORMAL:
	    		menu = new Menu();
	    		break;
	    	case Menu.ALTERNATIVO:
	    		menu = new MenuAlternativo();
	    		break;
	    }

		Calculadora c = new Calculadora(menu);
		c.rodar();
	}
}
class Menu{

	public static final int NORMAL = 1;
	public static final int ALTERNATIVO = 2;

	public void pedirA(){
		System.out.println("digite o valor A:");
	}

	public void pedirB(){
		System.out.println("digite o valor B:");
	}

	public void somar(){
		System.out.println("1) somar");
	}

	public void subtrair(){
		System.out.println("2) subtrair");
	}	

	public void sair(){
		System.out.println("3) sair");
	}
}

class MenuAlternativo extends Menu{

	@Override
	public void pedirA(){
		System.out.println("-------------- digite o valor A:");
	}

	public void pedirB(){
		System.out.println("-------------- digite o valor B:");
	}
}

class Calculadora{
	
	private Menu menu;

	public Calculadora(Menu m){
		this.menu = m;
	}

	public void rodar(){
		int i = 0;
		int a = 0;
		int b = 0;
		loop:
		while(i != 3){

			Scanner s = new Scanner(System.in);
			
			this.menu.pedirA();
			a = s.nextInt();

			this.menu.pedirB();
			b = s.nextInt();

			this.menu.somar();
			this.menu.subtrair();
			this.menu.sair();
			
			i = s.nextInt();
			switch(i){
				case 1:
					System.out.println(a+b);
					break;
				case 2:
					System.out.println(a-b);
					break;
				case 3:
					break loop;
			}
		}
	}
}