/*
	Usando a classe Pessoa, criar uma outra estrutura social.
	ex: 
		JogadorDeFutebol, Goleiro, Atacante, MeioCampo.
		Personagem, Vilao, Heroi, Coadjuvante
		Funcionario, Cobrador, Motorista, Fiscal
*/
class EstudanteProfessorExemplo{
	public static void main(String[] args){
		
		Pessoa[] pessoas = new Pessoa[3];
		
		Pessoa p1 = new Pessoa("Jean","Grey");
		pessoas[0] = p1;

		//estudante extende pessoa, logo é posível guardar um objeto
		//do tipo estudante em uma variável do tipo pessoa. O contrário
		//não é possível.
		Pessoa p2 = new Estudante("Scott","Summers");
		pessoas[1] = p2;

		//idem caso anterior, porém ao invés de estudante, a classe 
		//é professor.
		Pessoa p3 = new Professor("Charles","Xavier");
		pessoas[2] = p3;

		for(int i =0;i<pessoas.length;i++){
			//polimorfismo (professor e estudante, cada um implementam do seu jeito)
			//o método getInfo.
			String saida = pessoas[i].getInfo();
			System.out.println(saida);
		}

	}
}

class Pessoa{
	protected String nome;
	protected String sobrenome;

	public Pessoa(String nome, String sobrenome){
		this.nome = nome;
		this.sobrenome = sobrenome;
	}

	public String getInfo(){
		return "pessoa: "+this.nome+" "+this.sobrenome;
	}	
}

class Estudante extends Pessoa{
	
	public Estudante(String nome, String sobrenome){
		//super dentro de um método chama a versão ancestral com mesmo nome
		//na classe pai. Neste caso, estamos chamando o método Pessoa e passando
		//os mesmos parâmetros.
		super(nome, sobrenome);
	}

	//Marcação para deixar explícito que este método existe
	//na classe pai.
	//@Override
	public String getInfo(){
		return "estudante: "+this.nome+" "+this.sobrenome;
	}
}

class Professor extends Pessoa{
	
	public Professor(String nome, String sobrenome){
		super(nome, sobrenome);
	}

	@Override
	public String getInfo(){
		return "professor: "+this.nome+" "+this.sobrenome;
	}
}